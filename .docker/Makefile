NAME := xand-bank-mocks
FULL_NAME := gcr.io/xand-dev/$(NAME)

NPM_VERSION := $(shell cat ../package.json | jq -r .version)

ifeq ($(CI_COMMIT_REF_NAME),master)
	# Test with `CI_COMMIT_REF_NAME=master make print`
	VERSION := $(NPM_VERSION)
else ifeq ($(CI_COMMIT_REF_NAME),)
	# This is used during local development. 
	# Test with `make print`
	DATETIME := $(shell date +"%Y-%m-%d-%H-%M-%S" )
	VERSION := $(NPM_VERSION)-dev-$(DATETIME)
else
	# This is used for any non-master branch.
	# Test with `CI_COMMIT_REF_NAME=develop CI_PIPELINE_ID=1234 make print`
	VERSION := $(NPM_VERSION)-beta-$(CI_PIPELINE_ID)
endif

IMAGE_TAG := $(FULL_NAME):$(VERSION)
CONTAINER_NAME := $(NAME)

print:
	@echo "Name    : $(FULL_NAME)"
	@echo "Version : $(VERSION)"

build: print
	@echo "Building ${IMAGE_TAG}."

	docker build -f Dockerfile -t "${IMAGE_TAG}" ../
	docker tag ${IMAGE_TAG} $(FULL_NAME):latest

check-version: print
	./check-publish.sh $(FULL_NAME) $(NPM_VERSION)
	./check-publish.sh $(FULL_NAME) $(VERSION)

publish: print check-version
	docker push "${IMAGE_TAG}"

kill: print
	-docker rm -f $(CONTAINER_NAME)

run: print kill
	@echo "Running $(NAME) version ${VERSION}."
	docker run -it --rm --name $(CONTAINER_NAME) -p 8888:8888 "$(IMAGE_TAG)"

shell: print
	docker exec -it $(CONTAINER_NAME) /bin/bash

debug-gitlab: print
	@echo "PATH   : $(shell pwd)"
	@echo "HOME   : $(HOME)"
	@echo "USER   : $(USER)"
	@echo "DIR    : $(shell pwd)"
	@echo "PATH   : $(PATH)"
	@echo "src    : $(ls -al src)"