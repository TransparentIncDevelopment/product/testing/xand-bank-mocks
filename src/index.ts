#!/usr/bin/env node

import * as bodyParser from "body-parser";
import * as bunyan from "bunyan";
import * as express from "express";
import * as http from "http";
import { LATENCY_PATH } from "./constants";
import { BankConfig, BankProvider } from "./lib/Configuration";
import ConfigurationReader from "./lib/ConfigurationReader";
import MockBank from "./lib/MockBank";
import { APPLICATION_VERSION } from "./metadata";
import * as home from "./routes/home";
import Metropolitan from "./routes/metropolitan";
import Silvergate from "./routes/silvergate";
import TreasuryPrime from "./routes/treasury_prime";

const logger = bunyan.createLogger({name: "mock-service"});

let app: express.Application;
let httpServer: http.Server;
declare var latency_seconds: number;

export function start(port: number, initialReserveBalance = 10000000): Promise<http.Server> {
    app = express();

    const configLocations = ["/etc/xand/bank-mocks/config.yaml", "./config.yaml"];
    const configurationReader = new ConfigurationReader(configLocations);
    const configuration = configurationReader.get();
    configuration.initialReserveBalance = initialReserveBalance;

    app.locals.logger = logger;
    app.locals.configuration = configuration;
    // mount json form parser
    app.use(bodyParser.json({limit: "50mb"}));

    // mount query string parser
    app.use(bodyParser.urlencoded({
        extended: true,
    }));

    const bankConstructorForProviderType = (provider: BankProvider) => ({
        [BankProvider.Metropolitan]: Metropolitan,
        [BankProvider.Silvergate]: Silvergate,
        [BankProvider.TreasuryPrime]: TreasuryPrime,
    }[provider]);

    const makeBank =
        (bankConfig: BankConfig) => new (bankConstructorForProviderType(bankConfig.provider))(configuration, bankConfig);

    const banks = Object.assign({}, ...configuration.banks.map(
        (bankConfig) => ({
            [bankConfig.baseUrl]: makeBank(bankConfig),
        }),
    ));

    // Add middleware for extra latency
    app.use((req, res, next) => {
        if ( req.path === LATENCY_PATH) {
            return next();
        }
        if (globalThis.latency_seconds > 0) {
            logger.info(`Waiting ${ globalThis.latency_seconds } second(s)...`);
            setTimeout(() => {
                return next();
            }  , globalThis.latency_seconds * 1000);
        } else {
            return next();
        }
    });

    const router = createRouter(banks);
    app.use(router);

    return new Promise((resolve) => {
        httpServer = app.listen(port, () => {
            logger.info(`Bank mocks listening at ${port}`);
        });
        resolve(httpServer);
    });
}

function createRouter(banks: {[baseUrl: string]: MockBank<any, any>}): express.Router {
    const router = express.Router();

    for (const baseUrl of Object.keys(banks)) {
        const bank = banks[baseUrl];
        router.use("/" + baseUrl, bank.produceRouter());
    }

    router.use(home);

    // add liveness endpoint
    router.get("/health", (req, res, next) => {
        res.app.locals.logger.info("/health");
        res.send({
            status: "ok",
            uptime: process.uptime() * 1000,
            version: APPLICATION_VERSION,
            configuration: res.app.locals.configuration,
        });
        next();
    });
    return router;
}

export function stop(): Promise<void> {
    return new Promise<void>((resolve, reject) => {
        if (httpServer) {
            httpServer.close(() => {
                resolve();
            });

        } else {
            reject();
        }
    });
}

export function commandline() {

    const args = require("yargs")
        .option("port", {describe: "Server Port", default: 8888})
        .option("reserve-balance", {
            alias: "r",
            type: "number",
            description: "Set the intitial reserve balance at all banks to the provided value",
            default: 10000000,
        })
        .version(`Version : ${require("../package").version}`)
        .help("help").argv;

    start(args.port, args.reserveBalance);
}
