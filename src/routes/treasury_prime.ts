import { Buffer } from "buffer";
import * as bunyan from "bunyan";
import express = require("express");
import { stringify } from "flatted/cjs";
import * as fs from "fs";
import * as mustache from "mustache";
import { authenticationMiddleware } from "../lib/Authentication";
import Clock from "../lib/Clock";
import Configuration, { BankConfig } from "../lib/Configuration";
import MockBank from "../lib/MockBank";

const logger = bunyan.createLogger({name: "treasury-prime-mock"});
const reserveAcct = "9999999999";

const error =
    fs.readFileSync(__dirname + "/../templates/treasury_prime_error.json").toString();
const emptyData =
    fs.readFileSync(__dirname + "/../templates/treasury_prime_empty_account_response.json").toString();
const invalidParam =
    fs.readFileSync(__dirname + "/../templates/treasury_prime_invalid_param.json").toString();
const emptyQueryTempl =
    fs.readFileSync(__dirname + "/../templates/treasury_prime_empty_query.json").toString();
const accountsTempl =
    fs.readFileSync(__dirname + "/../templates/treasury_prime_account.json").toString();
const singleAccountTempl =
    fs.readFileSync(__dirname + "/../templates/treasury_prime_single_account.json").toString();
const bookTransferResponseTempl =
    fs.readFileSync(__dirname + "/../templates/treasury_prime_book_transfer_response.json").toString();
const missingRequiredParameterTempl =
    fs.readFileSync(__dirname + "/../templates/treasury_prime_missing_required_parameter_response.json").toString();
const invalidAccountIdTempl =
    fs.readFileSync(__dirname + "/../templates/treasury_prime_invalid_account_id_response.json").toString();
const bookTransferHistoryResponseTempl =
    fs.readFileSync(__dirname + "/../templates/treasury_prime_book_history_response.json").toString();
const invalidDateTimeTempl =
    fs.readFileSync(__dirname + "/../templates/treasury_prime_invalid_datetime.json").toString();
const emptyDateTimeTempl =
    fs.readFileSync(__dirname + "/../templates/treasury_prime_empty_datetime.json").toString();

interface BookTransferResponse {
    destinationAccount: string;
    amount: string;
    sourceAccount: string;
    datetimeUpdated: string;
    uniqueBookId: string;
    datetimeCreated: string;
    userdataMetadata: string;
}

// Type model for ingesting input
interface BookTransfer {
    destinationAccount: string;
    amount: string;
    sourceAccount: string;
    datetimeUpdated: Date;
    uniqueBookId: string;
    datetimeCreated: Date;
    userdata: JSON;
}

// View model for populating History JSON mustache template properly
interface BookTransferViewModel {
    txn: BookTransfer;
    createdDate: string;
    updatedDate: string;
    userdata: string;
    last: boolean;
}

export default class TreasuryPrime extends MockBank<BookTransfer, string> {

    private acctIds: Map<string, string>;
    private readonly clock: Clock;

    constructor(cfg: Configuration, bankConfig: BankConfig, clock = new Clock()) {
        super(cfg, bankConfig);
        this.acctIds = new Map();
        this.clock = clock;
        this.acctBalances.set(reserveAcct, this.initialReserveBalance);
    }

    public produceRouter(): express.Router {
        const app = express.Router();

        if (this.bankConfig?.authentication) {
            app.use(authenticationMiddleware(this.bankConfig.authentication));
        }

        app.get("", (req, res, next) => {
            res.send("up");
        });

        /**
         *  @api {get} /account Get account balance by account id
         *  @apiName GetBalanceByAccountId
         *  @apiGroup Balance
         *  @apiDescription Get balance from an account id
         *  @apiParam {String} id Account unique id
         *  @apiParamExample {String} Request-Example: account_number:12345
         *
         *  @apiSuccess (200) {Object} templated `accountsTempl` response object
         *  @apiError (400) {Object} templated `notFound` response object when account not found
         *  @apiError (400) {Object} templated `emptyData` response object when account id not supplied
         */
        app.get("/account/:id", this.accountByAccountId.bind(this));

        /**
         *  @api {get} /account Get account ids by account number
         *  @apiName GetAccountByAccountNumber
         *  @apiGroup Balance
         *  @apiDescription Account id is required to fetch balance using the account/:id endpoint.
         *  @apiParam {String} account_number
         *  @apiParamExample {String} Request-Example: id:acct_12345
         *
         *  @apiSuccess (200) {Object} templated `singleAccountTempl` response object
         *  @apiError (400) {Object} templated `emptyQueryTempl` response object when required query string not supplied
         *  @apiError (400) {Object} templated `invalidParam` response object account_number param not supplied
         *  @apiError (404) {Object} catch-all `not found` response object
         */
        app.get("/account", this.accountsByAccountNumber.bind(this));
        // app.get("/account", this.accountsByAccountNumber);

        /**
         * @api {post} /book Create book transfer
         * @apiName CreateBookTransfer
         * @apiGroup Book
         * @apiDescription Create a book transfer with indicated values
         * @apiParam {String} to_account_id
         * @apiParam {String} amount
         * @apiParam {String} from_account_id
         * @apiParam {Object} [userdata]
         * @apiParamExample {String} Request-Example: acct_12345
         * @apiParamExample {json} Request-Example:
         *     {
         *       "amount": "100000.01",
         *       "from_account_id": "acct_11evpq33s8nwk",
         *       "to_account_id": "acct_11evpq3xs8nwn",
         *       "userdata":
         *      {
         *       "metadata": "some-correlation-data-123"
         *       }
         *     }
         * @apiSuccess (200) {Object} templated `bookTransferResponseTempl` response object
         * @apiError (400) {Object} templated `missingRequiredParameterTempl`
         * response object when required param not supplied
         * @apiError (400) {Object} templated `invalidAccountIdTempl` response object where account id doesn't exist
         * @apiError (403) {Object} catch-all `forbidden` response object
         */
        app.post("/book", this.createBookTransferRequest.bind(this));

        /**
         * @api {get} /book Get book transfer history
         * @apiName GetHistory
         * @apiGroup Book
         * @apiDescription Get a history of book transfers
         * @apiParam {String} [from_date]
         * @apiParam {String} [to_date]
         * @apiParam {String} [page_cursor]
         *
         * @apiSuccess (200) {Object} templated `bookTransferHistoryResponseTempl` response object
         * @apiError (400) {Object} templated `invalidDateTimeTempl` response object when from_date or to_date values invalid
         * @apiError (400) {Object} templated `emptyDateTimeTempl` response object when from_date or to_date fields empty
         * @apiError (403) {Object} catch-all `forbidden` response object
         */
        app.get("/book", this.bookTransferHistory.bind(this));

        return app;
    }

    public accountsByAccountNumber(req: express.Request, res: express.Response,
                                   next: express.NextFunction) {
        logger.info(`Received accounts by account number request: ${stringify(req.query)}`);
        // Test sending a bad account number
        if (req.query.account_number === "") {
            const params = {fieldName: "account_number"};
            const result = mustache.render(emptyQueryTempl, params);
            res.send(JSON.parse(result));
            return;
        } else if (Object.keys(req.query)[0] !== "account_number") {
            const params = {fieldName: Object.keys(req.query)};
            const result = mustache.render(invalidParam, params);
            res.send(JSON.parse(result));
        } else {
            const accountNumber = String(req.query.account_number.toString());
            const accountId = "acct_" + accountNumber;
            this.acctIds.set(accountId, accountNumber);
            const params = {
                accountId,
                accountNumber,
            };
            const result = mustache.render(singleAccountTempl, params);
            res.send(JSON.parse(result));
            next();
        }
    }

    public accountByAccountId(req: express.Request, res: express.Response,
                              next: express.NextFunction) {
        logger.info(`Received account by account id request: ${stringify(req.query)}`);
        if (req.params.id === "bad-id") {
            const errorResult = mustache.render(error, {message: "Record with id bad-id not found."});
            res.status(404).send(JSON.parse(errorResult));
            return;
        } else {
            // Account numbers are written based on given id
            const accountId = String(req.params.id.toString());
            const accountNumber = accountId.includes("acct_")
                ? accountId.replace("acct_", "")
                : accountId.concat(accountId, "_acctnum");
            this.acctIds.set(accountId, accountNumber);

            let balance = this.acctBalances.get(accountId);

            if (balance === undefined) {
                balance = this.initialAccountBalance;

                this.acctBalances.set(accountId, balance);
            }

            const params = {
                accountId,
                accountNumber,
                balance: balance.toFixed(2),
            };
            const result = mustache.render(accountsTempl, params);
            res.send(JSON.parse(result));
            next();
        }
    }

    public createBookTransferRequest(req: express.Request, res: express.Response,
                                     next: express.NextFunction) {
        const requiredParams: string[] = [
            "to_account_id",
            "amount",
            "from_account_id",
            "userdata",
        ];
        let result;
        for (const param of requiredParams) {
            if (req.body.hasOwnProperty(param)) {
                if (!req.body && !req.body[param] && typeof req.body[param] === undefined && req.body[param] == null) {
                    // Empty request
                    return next();
                } else if (JSON.stringify(req.body[param]).includes("bad-id") && param.includes("account_id")) {
                    // Enables testing sending to or from an account with id = "bad-id"
                    logger.info(`Invalid account id: ${param}`);
                    result = mustache.render(invalidAccountIdTempl, param);
                    res.send(JSON.parse(result));
                    return next();
                }
            } else {
                // Param was not passed in
                logger.info(`Request missing required data: ${param}`);
                result = mustache.render(missingRequiredParameterTempl, param);
                res.status(400).send(JSON.parse(result));
                return next();
            }
        }

        logger.info(`Received valid book transfer request: ${stringify(req.body)}`);
        const {to_account_id, amount, from_account_id, userdata} = req.body;
        const clock = this.clock;
        const params: BookTransferResponse = {
            destinationAccount: to_account_id,
            amount,
            sourceAccount: from_account_id,
            datetimeUpdated: clock.now().toISOString(),
            uniqueBookId: "book_" + Math.random().toString(36).substr(2, 12),
            datetimeCreated: clock.now().toISOString(),
            userdataMetadata: JSON.stringify(userdata),
        };
        result = mustache.render(bookTransferResponseTempl, params);

        const bookTransferAmtAsNumber: number = +params.amount;
        const newBalanceForDest =
            twoDecimal(
                (this.acctBalances.has(params.destinationAccount) ?
                        this.acctBalances.get(params.destinationAccount) :
                        this.initialAccountBalance
                ) + bookTransferAmtAsNumber,
            );
        const newBalanceForSource =
            twoDecimal(
                (this.acctBalances.has(params.sourceAccount) ?
                        this.acctBalances.get(params.sourceAccount) :
                        this.initialAccountBalance
                ) - bookTransferAmtAsNumber,
            );
        this.acctBalances.set(params.destinationAccount, newBalanceForDest);
        this.acctBalances.set(params.sourceAccount, newBalanceForSource);
        this.seenTransactions.push({
            destinationAccount: params.destinationAccount,
            amount: params.amount,
            sourceAccount: params.sourceAccount,
            datetimeUpdated: new Date(params.datetimeUpdated),
            uniqueBookId: params.uniqueBookId,
            datetimeCreated: new Date(params.datetimeCreated),
            userdata: JSON.parse(params.userdataMetadata),
        });
        res.send(JSON.parse(result));
        return next();
    }

    public bookTransferHistory = (req: express.Request, res: express.Response, next: express.NextFunction) => {
        let result;
        for (const param of Object.keys(req.query)) {
            if (param.includes("date") && req.query[param] === "") {
                logger.info(`Empty date: ${param}`);
                result = mustache.render(emptyDateTimeTempl, {emptyDateTime: param});
                res.status(400).send(JSON.parse(result));
                return next();
            } else if (param.includes("date") && isNaN(new Date(req.query[param] as string | number).getTime())) {
                // Bad datetime format
                logger.info(`Invalid date-time: ${param}`);
                result = mustache.render(invalidDateTimeTempl, {badDateTime: param});
                res.status(400).send(JSON.parse(result));
                return next();
            }
        }

        logger.info(`Received valid book history request: ${stringify(req.query)}`);

        const pageCursor = req.query.page_cursor;
        const pageSize = Number(req.query.page_size ?? 100);
        const fromDate = req.query.from_date ? new Date(String(req.query.from_date)) : new Date(Date.UTC(0, 0));
        const toDate = req.query.to_date ? new Date(String(req.query.to_date)) : new Date(Date.UTC(10000, 0));

        const reverseChronoTxns = this.seenTransactions
            .filter((txn) => {
                const date = txn.datetimeCreated;
                return date >= fromDate && date < toDate;
            })
            .map((txn) => {
                const bookView: BookTransferViewModel = {
                    txn,
                    userdata: JSON.stringify(txn.userdata),
                    createdDate: txn.datetimeCreated.toISOString(),
                    updatedDate: txn.datetimeUpdated.toISOString(),
                    last: false,
                };
                return bookView;
            })
            .reverse();
        const pageIndexStart = pageCursor
            ? reverseChronoTxns.findIndex((x) => x.txn.uniqueBookId === pageCursor) + 1
            : 0;
        const pageIndexEnd = pageIndexStart + Math.min(pageSize, reverseChronoTxns.length - pageIndexStart);
        const pagedTxns = reverseChronoTxns.slice(pageIndexStart, pageIndexEnd);
        if (pagedTxns.length >= 1) {
            pagedTxns[pagedTxns.length - 1].last = true;
        }

        const estimatedTotalTxns = Math.ceil(reverseChronoTxns.length / 10) * 10;
        const hasMorePages = reverseChronoTxns.length > pageIndexEnd;
        const nextPage = hasMorePages
            ? `http://${req.headers.host}${req.baseUrl}${req.route.path}?page_cursor=${reverseChronoTxns[pageIndexEnd - 1].txn.uniqueBookId}`
            : false;
        result = mustache.render(
            bookTransferHistoryResponseTempl,
            {
                params: pagedTxns,
                est_total: estimatedTotalTxns,
                next_page: nextPage,
            });
        res.send(JSON.parse(result));
        return next();
    }
}

function twoDecimal(inputNumber: number) {
    return Math.round(inputNumber * 100) / 100;
}
