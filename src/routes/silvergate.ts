import * as bunyan from "bunyan";
import { Router } from "express";
import { stringify } from "flatted/cjs";
import * as fs from "fs";
import * as mustache from "mustache";
import Configuration, { BankConfig } from "../lib/Configuration";
import MockBank from "../lib/MockBank";

const logger = bunyan.createLogger({name: "silvergate-mock"});
const reserveAcct = 0;

const transTempl =
    fs.readFileSync(__dirname + "/../templates/sg_history_template.json").toString();
const balanceTempl =
    fs.readFileSync(__dirname + "/../templates/sg_balance_template.json").toString();

export default class Silvergate extends MockBank<any, number> {
    constructor(cfg: Configuration, bankConfig: BankConfig) {
        super(cfg, bankConfig);

        // While this mock API accepts strings for AccountIds, the are coerced to numbers so "0002" is
        // the same account as "2". "000X" shows up because the account info part of smelt transactions
        // are padded to fit in the bank_account_id field. Currently, the AccountId is handled as a
        // string everywhere, and that padding is not being interpreted/stripped anywhere but here.
        // See: https://www.pivotaltracker.com/n/projects/2353850/stories/166681127
        this.acctBalances.set(1033900117, this.initialAccountBalance);
        this.acctBalances.set(reserveAcct, this.initialReserveBalance);
    }

    public produceRouter(): Router {
        const app: Router = Router();

        app.get("/", (req, res) => {
            res.send("up");
        });
        app.get("/v2", (req, res) => {
            res.send("up");
        });
        app.get("/v2/access/token", this.token.bind(this));
        app.get("/v2/account/history", this.history.bind(this));
        app.post("/v2/account/transfersen", this.transfer.bind(this));
        app.get("/v2/account/balance", this.balance.bind(this));
        return app;
    }

    public token(req: any, res: any, next: any) {
        logger.info(`Got token request`);
        res.header("Authorization", "TOTALLY A REAL TOKEN");
        res.send();
        next();
    }

    public history(req: any, res: any, next: any) {
        logger.info(`SG history request: ${stringify(req.query)}`);
        const account = Number(req.query.accountNumber);
        const transactions = this.seenTransactions
            .filter((t) => (Number(t.AccountNumberTo) === account ||
                Number(t.AccountNumberFrom) === account))
            .map((t) => {
                return {
                    trans_amnt: t.Amount,
                    trans_id: t.AccountFromDescription2,
                    trans_type: Number(t.AccountNumberFrom) === account ? "D" : "C",
                    last: false,
                };
            });
        if (transactions.length > 0) {
            transactions[transactions.length - 1].last = true;
        }
        const params = {
            num_transactions: this.seenTransactions.length,
            transactions,
        };
        const result = mustache.render(transTempl, params);
        res.send(JSON.parse(result));
        next();
    }

    public transfer(req: any, res: any, next: any) {
        logger.info(`SG transfer request: ${stringify(req.body)}`);
        if (!req.body.hasOwnProperty("SequenceNumber") ||
            !req.body.hasOwnProperty("Amount") ||
            !req.body.hasOwnProperty("AccountNumberFrom") ||
            !req.body.hasOwnProperty("AccountNumberTo") ||
            !req.body.hasOwnProperty("AccountFromDescription2") ||
            !req.body.hasOwnProperty("AccountToDescription2")) {
            res.send(400, "Invalid TX");
            next();
            return;
        }

        const fromAcct = Number(req.body.AccountNumberFrom.toString());
        const toAcct = Number(req.body.AccountNumberTo.toString());
        const amount = parseFloat(req.body.Amount);

        if (!this.acctBalances.has(fromAcct)) {
            this.acctBalances.set(fromAcct, this.initialAccountBalance);
        }
        if (!this.acctBalances.has(toAcct)) {
            this.acctBalances.set(toAcct, this.initialAccountBalance);
        }
        if (this.acctBalances.get(fromAcct) - amount < 0) {
            logger.warn("Attempted transfer that would result in negative balance", req.body);
            res.status(401).send({
                ERROR: [{
                    ERRORMSG: "Not enough funds to cover transfer",
                }],
            });
            next();
            return;
        }
        this.acctBalances.set(fromAcct, this.acctBalances.get(fromAcct) - amount);
        this.acctBalances.set(toAcct, this.acctBalances.get(toAcct) + amount);

        const seqid = req.body.SequenceNumber;
        this.seenTransactions.push(req.body);
        const resp = {
            SEQUENCE: seqid,
            MESSAGE: [{
                MESSAGEID: "AL00000",
                MESSAGETYPE: "S",
                MESSAGETEXT: "Completed Successfully",
            }],
            ERROR: null,
        };

        logger.info("New transfer: ", req.body);
        res.send(resp);
        next();
    }

    public balance(req: any, res: any, next: any) {
        logger.info(`SG balance request: ${stringify(req.query)}`);
        if (!req.query.hasOwnProperty("accountNumber")) {
            res.status(400).send("Invalid TX");
            next();
            return;
        }

        const account = Number(req.query.accountNumber);

        let balance = this.acctBalances.get(account);

        if (balance === undefined) {
            balance = this.initialAccountBalance;
            this.acctBalances.set(account, balance);
        }

        const params = {
            account,
            balance,
        };

        const result = mustache.render(balanceTempl, params);
        res.send(JSON.parse(result));
        next();
    }

}
