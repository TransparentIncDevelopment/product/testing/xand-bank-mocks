import fs = require("fs") ;
import mergeYaml = require("merge-yaml");
import Configuration from "./Configuration";

export default class ConfigurationReader {

    private paths: string[];

    constructor(paths: string[]) {
        this.paths = paths;
    }

    public get(): Configuration {
        const config = this.getConfigFromFiles();

        const result = new Configuration();
        if (config.banks) {
            if (!Array.isArray(config.banks)) {
                throw new Error("Invalid configuration: \"banks\" must be array specifying list of banks");
            }

            const banks = config.banks as any[];

            result.banks = banks.map((configObject) => {
                const provider = Configuration.getProviderByName(configObject.provider);
                if (provider === null) {
                    throw new Error(`Invalid configuration: provider name "${configObject.provider}" unsupported`);
                }

                const baseUrl =
                    configObject["base-url"] ?? Configuration.getDefaultBaseUrlForProvider(provider);

                return {
                    provider,
                    baseUrl,
                    authentication: configObject.authentication ?? null,
                };
            });
        } else {
            result.banks = Configuration.getDefaultBanksSetting();
        }

        result.assertValid();

        return result;
    }

    private getConfigFromFiles(): any {
        const paths = this.paths.filter((path) => fs.existsSync(path));
        if (paths.length) {
            return mergeYaml(paths);
        }
        return {
            banks: null,
        };
    }
}
