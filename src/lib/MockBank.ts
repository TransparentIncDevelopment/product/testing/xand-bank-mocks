import { Router } from "express";
import Configuration, { BankConfig } from "./Configuration";

export default abstract class MockBank<TransactionType, AccountIdType> {
    // Initial balance all accounts begin with
    public initialAccountBalance: number;
    // Initial balance the trust's reserve account begins with
    public initialReserveBalance: number;
    // Current balances of all accounts (account num/id -> balance)
    public acctBalances: Map<AccountIdType, number>;
    // All transactions that have been recorded so far
    public seenTransactions: TransactionType[];
    public bankConfig: BankConfig;

    protected constructor(cfg: Configuration, bankConfig: BankConfig) {
        this.initialReserveBalance = cfg.initialReserveBalance;
        this.initialAccountBalance = 10000000;
        this.acctBalances = new Map();
        this.seenTransactions = [];
        this.bankConfig = bankConfig;
    }

    public abstract produceRouter(): Router;
}
