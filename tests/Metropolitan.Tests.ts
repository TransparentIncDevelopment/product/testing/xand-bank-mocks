import bodyParser = require("body-parser");
import { expect } from "chai";
import express = require("express");
import request = require("supertest");
import Configuration, { BankConfig, BankProvider } from "../src/lib/Configuration";
import Metropolitan from "../src/routes/metropolitan";

const DEFAULT_METROPOLITAN_CONFIG = {
    provider: BankProvider.Metropolitan,
    baseUrl: "metropolitan",
    authentication: null,
};

function metropolitan(bankConfig: BankConfig = DEFAULT_METROPOLITAN_CONFIG) {
    const testMe = new Metropolitan(new Configuration(), bankConfig);
    const app = express();
    app.use(bodyParser.json());
    app.use(testMe.produceRouter());
    return request(app);
}

describe("Given metropolitan bank mock which requires token retrieval authentication", () => {
    const app = metropolitan({ ...DEFAULT_METROPOLITAN_CONFIG, authentication: { username: "my-fake-username", password: "my-fake-password"} });

    describe("When I request a token using correct authentication", () => {
        const requestToken = () => app
            .post("/getAccessToken")
            .auth("my-fake-username", "my-fake-password")
            .send();

        it("responds with a success code and included token", async () => {
            await requestToken()
                .expect(200, {
                    access_token: "MOCK_MCB_BEARER_TOKEN",
                    token_type: "Bearer",
                    expires_in: 86400,
                });
        });
    });

    describe("When I request a token using incorrect username", () => {
        const requestToken = () => app
            .post("/getAccessToken")
            .auth("this-is-wrong", "my-fake-password")
            .send();

        it("responds with a 401 status code", async () => {
            await requestToken()
                .expect(401);
        });
    });

    describe("When I request a token using incorrect password", () => {
        const requestToken = () => app
            .post("/getAccessToken")
            .auth("my-fake-username", "this-is-wrong")
            .send();

        it("responds with a 401 status code", async () => {
            await requestToken()
                .expect(401);
        });
    });
});

describe("Given metropolitan bank mock without token retrieval authentication", () => {
    describe("When I attempt to create transactions", () => {
        it("Returns OK status code if the description is 33 characters", async () => {
            await metropolitan()
                .post("/Transfers")
                .auth("MOCK_MCB_BEARER_TOKEN", {type: "bearer"})
                .send(transaction("1111", "2222", "a".repeat(33)))
                .expect(200);
        });

        it("Returns an error if the description is 34 characters", async () => {
            await metropolitan()
                .post("/Transfers")
                .auth("MOCK_MCB_BEARER_TOKEN", {type: "bearer"})
                .send(transaction("1111", "2222", "a".repeat(34)))
                .expect(503)
                .expect(hasStatusDescription("Invalid Value: AccountFrom:TransferDescription cannot exceed 33 characters"));
        });
    });

    describe("Given the default page size is 10 and there are 11 transaction", () => {
        const app = metropolitan();
        before(async () => {
            const transactionCreations = [...Array(11)].map(async () => {
                await app
                    .post("/Transfers")
                    .auth("MOCK_MCB_BEARER_TOKEN", {type: "bearer"})
                    .send(transaction("1111", "2222"))
                    .expect(200);
            });

            await Promise.all(transactionCreations);
        });

        const history = (cursor) => app
                .post("/AcctTrn")
                .auth("MOCK_MCB_BEARER_TOKEN", {type: "bearer"})
                .send(historyRequest("1111", cursor));

        describe("When I request transfers using cursor = 0", () => {
            const cursor = 0;

            it("Then the first 10 transactions are returned", async () => {
                await history(cursor)
                    .expect(hasNumberTransactions(10));
            });
        });

        describe("When I request transfers using cursor = 1", () => {
            const cursor = 1;

            it("Then the single last remaining transaction is returned", async () => {
                await history(cursor)
                    .expect(hasNumberTransactions(1));
            });
        });

        const incorrectTokenHistory = (cursor) => app
                .post("/AcctTrn")
                .auth("this-is-very-wrong", {type: "bearer"})
                .send(historyRequest("1111", cursor));

        describe("When I request transfers using incorrect token", () => {
            it("Then a 401 status code is returned", async () => {
                await incorrectTokenHistory(0)
                    .expect(401);
            });
        });

        const unauthenticatedHistory = (cursor) => app
                .post("/AcctTrn")
                .send(historyRequest("1111", cursor));

        describe("When I request transfers using no token", () => {
            it("Then a 401 status code is returned", async () => {
                await incorrectTokenHistory(0)
                    .expect(401);
            });
        });
    });

    describe("Given an account request", () => {
        const requestAccount = (id) => metropolitan()
            .post("/Accounts")
            .auth("MOCK_MCB_BEARER_TOKEN", {type: "bearer"})
            .send(account(id));

        it("Responds with OK status code", async () => {
            await requestAccount("1111")
                .expect(200);
        });

        it("Responds with a current balance of $10,000,000", async () => {
            await requestAccount("1111")
                .expect(200)
                .expect(hasBalance("Current", 10_000_000));
        });

        it("Responds with an available balance of $10,000,000", async () => {
            await requestAccount("1111")
                .expect(200)
                .expect(hasBalance("Avail", 10_000_000));
        });
    });

    describe("Given an account with an initial balance of 10000000", () => {
        const app = metropolitan();

        it("Responds with OK status code", async () => {
            await app.post("/Accounts")
                .auth("MOCK_MCB_BEARER_TOKEN", {type: "bearer"})
                .send(account("1111"))
                .expect(200)
                .expect(hasBalance("Current", 10000000));

            await app
                .post("/Transfers")
                .auth("MOCK_MCB_BEARER_TOKEN", {type: "bearer"})
                .send(transaction("1111", "2222", "c".repeat(33), 10000000))
                .expect(200);

            await app.post("/Accounts")
                .auth("MOCK_MCB_BEARER_TOKEN", {type: "bearer"})
                .send(account("1111"))
                .expect(200)
                .expect(hasBalance("Current", 0));
        });
    });
});

function transaction(from, to, description: string = "some description", amount = 1.00) {
    return {
        XferReq: {
            // "EFXHdr" omitted
            XferInfo: {
                FromAcctRef: {
                    AcctKeys: {
                        AcctId: from,
                    },
                },
                ToAcctRef: {
                    AcctKeys: {
                        AcctId: to,
                    },
                },
                CurAmt: {
                    Amt: amount,
                },
                XferToDesc: description,
            },
        },
    };
}

function account(accountId: string) {
    return {
        AcctInqRq: {
            // "EFXHdr" omitted
            AcctSel: {
                AcctId: accountId,
                AcctType: {
                    AcctTypeValue: "DDA",
                    AcctTypeSource: "IFX",
                },
            },
        },
    };
}

function historyRequest(accountId, cursor) {
    return {
        AcctTrnInqRq: {
            // "EFXHdr" omitted
            AcctTrnSel: {
                AcctKeys: {
                    AcctId: accountId,
                },
            },
            RecCtrlIn: {
                Cursor: cursor,
            },
        },
    };
}

function hasNumberTransactions(expectedCount) {
    return (res) =>
        expect(res.body.AcctTrnInqRs.AcctTrnRec.length).to.equal(expectedCount);
}

function hasBalance(source: string, amount: number) {
    return (res) => {
        const allBalanceEntries: any[] = res.body?.AcctInqRs?.AcctRec?.AcctInfo?.AcctBal;
        expect(allBalanceEntries).to.be.an("array");

        const possibleTargetEntries = allBalanceEntries
            .filter((val) => val.BalType?.BalTypeSource === source);
        expect(possibleTargetEntries).to.be.an("array").with.lengthOf(1);

        expect(Number.parseFloat(possibleTargetEntries[0]?.CurAmt.Amt)).to.equal(amount);
    };
}

function hasStatusDescription(description: string) {
    return (res) => {
        expect(res.body?.XferRes?.EFXStatus).is.not.undefined;
        expect(res.body.XferRes.EFXStatus["ns1:ServerStatusDesc"]).to.equal(description);
    };
}
