import { expect } from "chai";

import fs = require("fs") ;
import Configuration, { BankProvider } from "../src/lib/Configuration";
import ConfigurationReader from "../src/lib/ConfigurationReader";

describe("Configuration", () => {

    describe("When config file does not exist", () => {
        let configuration: Configuration;

        before(() => {
            const configFile = "tests.config.yaml";

            if (fs.existsSync(configFile)) {
                fs.unlinkSync(configFile);
            }

            const reader = new ConfigurationReader([configFile]);
            configuration = reader.get();
        });

        it("Then all banks are enabled", () => {
            expect(configuration.banks).to.deep.equal([
                {
                    provider: BankProvider.Metropolitan,
                    baseUrl: "metropolitan",
                    authentication: null,
                },
                {
                    provider: BankProvider.Silvergate,
                    baseUrl: "silvergate",
                    authentication: null,
                },
                {
                    provider: BankProvider.TreasuryPrime,
                    baseUrl: "provident",
                    authentication: null,
                },
            ]);
        });
    });

    describe("When there are no config files", () => {
        let configuration: Configuration;

        before(() => {
            const reader = new ConfigurationReader([]);
            configuration = reader.get();
        });

        it("Then all banks are enabled", () => {
            expect(configuration.banks).to.deep.equal([
                {
                    provider: BankProvider.Metropolitan,
                    baseUrl: "metropolitan",
                    authentication: null,
                },
                {
                    provider: BankProvider.Silvergate,
                    baseUrl: "silvergate",
                    authentication: null,
                },
                {
                    provider: BankProvider.TreasuryPrime,
                    baseUrl: "provident",
                    authentication: null,
                },
            ]);
        });
    });

    describe("Given a local config file with default base URLs", () => {

        before(() => {
            const configYaml = `
# If not specified, all banks are enabled.
# If specified, only the specified banks are enabled.
banks:
  - provider: metropolitan
  - provider: treasury_prime
`;
            fs.writeFileSync("tests.config.yaml", configYaml);
        });

        describe("When I parse the config file", () => {

            let configuration: Configuration;

            before(() => {
                const paths: string[] = ["tests.config.yaml"];
                const reader = new ConfigurationReader(paths);
                configuration = reader.get();
            });

            it("Then metropolitan and treasury prime are enabled", () => {
                expect(configuration.banks).to.deep.equal([
                    {
                        provider: BankProvider.Metropolitan,
                        baseUrl: "metropolitan",
                        authentication: null,
                    },
                    {
                        provider: BankProvider.TreasuryPrime,
                        baseUrl: "treasury_prime",
                        authentication: null,
                    },
                ]);
            });
        });

    });

    describe("Given a config file with invalid provider key", () => {

        before(() => {
            const configYaml = `
# If not specified, all banks are enabled.
# If specified, only the specified banks are enabled.
banks:
  - provider: metropolitan
  - provider: foobar
  - provider: silvergate
`;
            fs.writeFileSync("tests.config.yaml", configYaml);
        });

        describe("When I parse the config file", () => {
            it("Then parsing the file throws an error", () => {
                const paths: string[] = ["tests.config.yaml"];
                const reader = new ConfigurationReader(paths);
                expect(reader.get.bind(reader)).to.throw("Invalid configuration: provider name \"foobar\" unsupported");
            });
        });
    });

    describe("Given a config file with specified authentication", () => {

        before(() => {
            const configYaml = `
# If not specified, all banks are enabled.
# If specified, only the specified banks are enabled.
banks:
  - provider: metropolitan
    authentication:
      username: foo
      password: bar
  - provider: silvergate
`;
            fs.writeFileSync("tests.config.yaml", configYaml);
        });

        describe("When I parse the config file", () => {
            let configuration: Configuration;

            before(() => {
                const paths: string[] = ["tests.config.yaml"];
                const reader = new ConfigurationReader(paths);
                configuration = reader.get();
            });

            it("Then metropolitan bank has authentication config", () => {
                expect(configuration.banks).to.deep.equal([
                    {
                        provider: BankProvider.Metropolitan,
                        baseUrl: "metropolitan",
                        authentication: { username: "foo", password: "bar" },
                    },
                    {
                        provider: BankProvider.Silvergate,
                        baseUrl: "silvergate",
                        authentication: null,
                    },
                ]);
            });
        });
    });

    describe("Given multiple config files", () => {

        before(() => {
            const initialYaml = `
# If not specified, all banks are enabled.
# If specified, only the specified banks are enabled.
banks:
  - provider: metropolitan
`;

            const overrideYaml = `
banks:
  - provider: silvergate
`;

            fs.writeFileSync("test1.config.yaml", initialYaml);
            fs.writeFileSync("test2.config.yaml", overrideYaml);
        });

        describe("When I parse the config file", () => {

            let configuration: Configuration;

            before(() => {
                const paths: string[] = ["test1.config.yaml", "test2.config.yaml"];
                const reader = new ConfigurationReader(paths);
                configuration = reader.get();
            });

            it("Then only silvergate is enabled and metropolitan is disabled", () => {
                expect(configuration.banks).to.deep.equal([
                    {
                        provider: BankProvider.Silvergate,
                        baseUrl: "silvergate",
                        authentication: null,
                    },
                ]);
            });
        });
    });

    describe("Given a config file with multiple Treasury Prime instances", () => {

        before(() => {
            const configYaml = `
banks:
  - provider: treasury_prime
    base-url: onebank
  - provider: treasury_prime
    base-url: anotherbank
`;
            fs.writeFileSync("tests.config.yaml", configYaml);
        });

        describe("When I parse the config file", () => {
            let configuration: Configuration;

            before(() => {
                const paths: string[] = ["tests.config.yaml"];
                const reader = new ConfigurationReader(paths);
                configuration = reader.get();
            });

            it("Then both banks are configured with different routes", () => {
                expect(configuration.banks).to.deep.equal([
                    {
                        provider: BankProvider.TreasuryPrime,
                        baseUrl: "onebank",
                        authentication: null,
                    },
                    {
                        provider: BankProvider.TreasuryPrime,
                        baseUrl: "anotherbank",
                        authentication: null,
                    },
                ]);
            });
        });
    });

    describe("Given a config file with a custom route", () => {

        before(() => {
            const configYaml = `
banks:
  - provider: treasury_prime
    base-url: abank
`;
            fs.writeFileSync("tests.config.yaml", configYaml);
        });

        describe("When I parse the config file", () => {
            let configuration: Configuration;

            before(() => {
                const paths: string[] = ["tests.config.yaml"];
                const reader = new ConfigurationReader(paths);
                configuration = reader.get();
            });

            it("Then the configured bank has a custom route", () => {
                expect(configuration.banks).to.deep.equal([
                    {
                        provider: BankProvider.TreasuryPrime,
                        baseUrl: "abank",
                        authentication: null,
                    },
                ]);
            });
        });
    });

    describe("Given a config file with malformed route", () => {
        before(() => {
            const configYaml = `
banks:
  - provider: treasury_prime
    base-url: "some/thing"
`;
            fs.writeFileSync("tests.config.yaml", configYaml);
        });

        describe("When I parse the config file", () => {
            it("Then parsing the file throws an error", () => {
                const paths: string[] = ["tests.config.yaml"];
                const reader = new ConfigurationReader(paths);
                expect(reader.get.bind(reader)).to.throw("Invalid configuration: base URL \"some/thing\" must only contain letters, numbers, dashes and underscores, and be at least one character long");
            });
        });
    });
});
