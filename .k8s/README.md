# Understanding this k8s configuration

These kubernetes files are designed for consumption by Octopus Deploy. Values in the `yaml` that will change between environments are templated with Octopus variables (e.g., #{kubernetes-namespace}) They are packaged into a semantically versioned zipfile and published to Octopus. Octopus will perform variable substution before applying them to the cluster.

## Required Tools

* jq
* zip
* kubectl 1.17 or greater

```bash
sudo apt install jq zip

sudo apt-get update && sudo apt-get install -y apt-transport-https
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
echo "deb https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee -a /etc/apt/sources.list.d/kubernetes.list
sudo apt-get update
sudo apt-get install -y kubectl
```

If you are testing publishing to Octopus, you will need an `$(OCTOPUS_API_KEY)`.

## Basic Workflow

```mermaid
sequenceDiagram
    engineer-->>CLI: Execute make build
    CLI-->>local: Produce k8s files ready for deployment
    engineer-->>CLI: Execute make deploy
    CLI-->>k8s: Execute the deployment
```

```bash
make build
bat .output/$(USER).yaml # you are using bat instead of cat right?
make deploy
```

## Make Targets

These make targets are available in the `.k8s` directory.

| Command | Description |
|--|--|
| print | echoes the variables that will be used for other targets to `stdout`. |
| clean | Removes the `.output` directory and contents |
| kustomize | Runs `kubectl kustomize` on the k8s yamls, producing `.output/$(USER).yaml`. |
| configure | Mimics Octopus' variable substution on the $(OUTPUT_FILE). This is really just `sed` replacements so it won't do the more sophisticated variable manipulation supported by Octopus such as `each` and `if` operations. |
| build | Runs the `print`, `kustomize`, and `configure` targets to produce a valid k8s document ready for deployment to the `$USER`s namespace. |
| deploy | runs `kubectl apply` on the $(OUTPUT_FILE) |
| package | Produces a package ready to be published into Octopus. If run locally, the package will be specific to $USER. If run in `gitlab`, the package will be `beta` on any non-master branch, and `release` on master. |
| publish | Publishes the package to Octopus. This target does not currently support overwriting packages (which should only be done locally, when at all). It requires that you have defined an $(OCTOPUS_API_KEY) |

One or more targets can be strung together. 

```bash
make clean build deploy

rm -rf .output
CONFIGURATION
Image Version      : '1.4.5'
Image              : 'gcr.io/xand-dev/xand-bank-mocks:1.4.5'
Environment        : 'crmckenzie'
Output File        : '.output/crmckenzie.yaml'
Package Version    : '1.4.5-crmckenzie'
Package            : '.output/xand-bank-mocks-k8s.1.4.5-crmckenzie.zip'
CI_COMMIT_REF_NAME : ''
CI_PIPELINE_ID     : ''

mkdir -p .output
kubectl kustomize base > .output/crmckenzie.yaml
sed -i 's/#{service-name}/xand-bank-mocks/g' .output/crmckenzie.yaml
sed -i 's/#{kubernetes.namespace}/crmckenzie/g' .output/crmckenzie.yaml
sed -i 's/#{docker.image.version}/1.4.5/g' .output/crmckenzie.yaml
sed -i 's/#{docker.image.name}/gcr.io\/xand-dev\/xand-bank-mocks:1.4.5/g' .output/crmckenzie.yaml
sed -i 's/#{deployment.datetime}/2020-03-25 09:28:10/g' .output/crmckenzie.yaml
kubectl apply -f .output/crmckenzie.yaml
namespace/crmckenzie configured
configmap/xand-bank-mocks-config-map-89628fkmh5 configured
service/xand-bank-mocks created
deployment.apps/xand-bank-mocks created
ingress.extensions/xand-bank-mocks-ingress created
```

## Octopus Variables

| Variable Name | Meaning |
| -- | -- |
| kubernetes.namespace | The kubernetes namespace into which the resources will be deployed. |
| service-name | The name prefix for most resources created in kubernetes. |
| docker.image.name | The fully qualified name of the docker image. |
| docker.image.version | The version of the docker container being deployed. |
| deployment.datetime | The timestamp at which Octopus is performing the deployment. |
