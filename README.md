# Bank Mock Server

This is a simple node REST server that imitates banking APIs.

All accounts have $10,000,000 in them by default (any string is a valid account).

Requests against it are performed exactly like they would be against a real bank api, but over http
rather than https.

If an error is thrown it will appear in the logs, as well as in the response body.

Start it with `yarn build && yarn start`.

The local server is set to listen at port 8888.

Test locally at:
```
localhost:<port number>/<endpoint>
#e.g.: localhost:8888/provident
```

## Configuration

Banks can be configured with the `banks` key in `config.yaml`. Valid structure looks as follows:

```yaml
banks:
  - provider: treasury_prime
    base-url: provident
    authentication:
      username: someusername
      password: somepassword
```

`base-url` and `authentication` are optional, and will default to the provider name and no
authentication, respectively.

Valid values for `provider` are `metropolitan`, `treasury_prime` and `silvergate`.

## APIs

Each bank mock will be hosted at `/<base-url>`, where `<base-url>` is the value provided in
configuration.

Health status, uptime and a configuration dump are available at `/health`.

## Bank metadata

### Silvergate Bank

Provider: `silvergate`
routing number: 928173892
reserve account: 0000000000

### Metropolitan Commercial Bank

Provider: `metropolitan`
routing number: 121141343
reserve account: 5555555555

### Provident (via Treasury Prime)

Provider: `treasury_prime`
routing number: 211374020
reserve account: 9999999999

## Testing
The mocks provide additional configuration to help with testing. 

### Add Latency
Use the `/latency` endpoint with the `seconds` parameter in the body to add x seconds of latency to all the bank 
endpoint calls. 

Example:
```
curl -d "seconds=4" -X POST localhost:8888/latency
```
Will set the latency for all the calls to 4 seconds.